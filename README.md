<p style="align:center; background-color:#1e1d32;">
  <img src="http://hackathon.samarindakota.go.id/img/logo_home.png" width="150" title="Samarinda Hackathon 2.0">
</p>

# Samarinda Hackathon 2.0
Samarinda Hackathon adalah kompetisi pengembangan aplikasi bisnis digital, dengan tema "Smart Citra Niaga", menggunakan berbagai API dari platform digital Pemerintah Kota Samarinda yang disediakan oleh Diskominfo Kota Samarinda.

## Mulai

* Silahkan Anda salin proyek pada [Git Samarinda Hackathon](http://git.samarindakota.go.id/smd.hackathon) pilih Repository berdasarkan Nomor Tim Anda untuk tujuan pengembangan dan pengujian.
* Request access Repository
* Konfirmasi ke Panitia
* happy coding :)

## Prasyarat

Software yang diperlukan

* Download Github For [OSX](https://git-scm.com/download/mac)
* Download Github For [Windows](https://gitforwindows.org/)
* Download Github For [Linux](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

## Penggunaan

Git global setup
```bash
git config --global user.name "usernameAnda"
git config --global user.email "emailAnda@domain.com"
```

Salin repository [Git Samarinda Hackhathon](http://git.samarindakota.go.id/smd.hackathon) berdasarkan nomor tim anda.

Menyalin Repo:
```bash
$ git clone http://git.samarindakota.go.id/smd.hackathon/tim-17.git
$ cd tim-17
```
Update repository dengan working file baru
```bash
$ git add <filename> atau git add .
$ git commit -m "add index"
$ git push origin master
```
Update repository lokal dengan commit terbaru pada Git Samarinda Hackhathon
```bash
$ git pull origin master
```